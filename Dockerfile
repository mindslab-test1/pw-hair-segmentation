FROM nvcr.io/nvidia/pytorch:19.06-py3

WORKDIR /workspace

COPY . ./

RUN pip install -r requirements.txt
RUN python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. hair_info.proto

EXPOSE 15001
CMD ["python", "server.py"]
