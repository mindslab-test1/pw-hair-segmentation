import grpc

from hair_info_pb2 import InputImageBytes
from hair_info_pb2_grpc import HairInfoStub


TEST_IMG = "a.jpg"


def img_iter(path):
    with open(path, "rb") as f:
        image_bytes = f.read()
        for idx in range(0, len(image_bytes), 1024):

            yield InputImageBytes(
                input=image_bytes[idx:idx + 1024]
            )


if __name__ == '__main__':
    with grpc.insecure_channel("182.162.19.14:30001") as channel:
        stub = HairInfoStub(channel)
        result = b""
        for result_part in stub.GetHairInfo(img_iter(TEST_IMG)):
            if result_part.info == "":
                result += result_part.image
            else: print(result_part.info)

        with open("server_out.jpg", "wb") as f:
            f.write(result)
