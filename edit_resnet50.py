import torch
import torch.nn as nn
import torchvision.models as models
import torch.nn.functional as F


class change_resnet50(nn.Module):
    def __init__(self, num_classes, num_fcs=0):
        super(change_resnet50, self).__init__()
        resnet50_copy = models.resnet50()
        resnet50_copy.conv1 = nn.Conv2d(4, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.base = nn.Sequential(*list(resnet50_copy.children())[:-2])
        self.num_fcs = num_fcs
        for i in range(num_fcs):
            setattr(self, "fc%d" % i, nn.Linear(2048, num_classes[i]))

    def forward(self, x):
        x = self.base(x)
        x = F.avg_pool2d(x, x.size()[2:])
        f = x.view(x.size(0), -1)
        clf_outputs = {}
        for i in range(self.num_fcs):
            clf_outputs["fc%d" % i] = getattr(self, "fc%d" % i)(f)

        return clf_outputs


