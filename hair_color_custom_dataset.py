from torch.utils.data import Dataset
from PIL import Image
import torchvision.transforms as transforms
import numpy as np
import torch


class HairInfoDataset(Dataset):
    def __init__(self, dataset, image_dir, txt_dir):
        self.image_frame = dataset
        self.image_dir = image_dir
        self.txt_dir = txt_dir

    def __len__(self):
        return len(self.image_frame)

    def __getitem__(self, idx):
        img_dir = self.image_dir + self.image_frame[idx]
        txt_dir = self.txt_dir + self.image_frame[idx]

        t = open(txt_dir + ".txt", 'r')
        lines = t.readlines()
        token = []
        for i in lines:
            token.append(i[-2])
            break

        txt_result = token.copy()
        txt_result = list(map(int, txt_result))

        image = Image.open(img_dir +'.jpg')
        img_result = np.array(image)
        txt_result = np.array(txt_result)

        hair_dataset = {'image': img_result, 'label': txt_result}
        transform_image_data = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5),(0.5, 0.5, 0.5))
        ])

        hair_dataset['image'] = transform_image_data(hair_dataset['image'])
        hair_dataset['label'] = torch.squeeze(torch.Tensor(list(hair_dataset['label'])))

        return hair_dataset