# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hair_info.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='hair_info.proto',
  package='maum.pa.hair.info',
  syntax='proto3',
  serialized_options=b'\n\021maum.pa.hair.info',
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x0fhair_info.proto\x12\x11maum.pa.hair.info\" \n\x0fInputImageBytes\x12\r\n\x05input\x18\x01 \x01(\x0c\"2\n\x13OutputImageWithInfo\x12\x0c\n\x04info\x18\x01 \x01(\t\x12\r\n\x05image\x18\x02 \x01(\x0c\x32i\n\x08HairInfo\x12]\n\x0bGetHairInfo\x12\".maum.pa.hair.info.InputImageBytes\x1a&.maum.pa.hair.info.OutputImageWithInfo(\x01\x30\x01\x42\x13\n\x11maum.pa.hair.infob\x06proto3'
)




_INPUTIMAGEBYTES = _descriptor.Descriptor(
  name='InputImageBytes',
  full_name='maum.pa.hair.info.InputImageBytes',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='input', full_name='maum.pa.hair.info.InputImageBytes.input', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=38,
  serialized_end=70,
)


_OUTPUTIMAGEWITHINFO = _descriptor.Descriptor(
  name='OutputImageWithInfo',
  full_name='maum.pa.hair.info.OutputImageWithInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='info', full_name='maum.pa.hair.info.OutputImageWithInfo.info', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='image', full_name='maum.pa.hair.info.OutputImageWithInfo.image', index=1,
      number=2, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=72,
  serialized_end=122,
)

DESCRIPTOR.message_types_by_name['InputImageBytes'] = _INPUTIMAGEBYTES
DESCRIPTOR.message_types_by_name['OutputImageWithInfo'] = _OUTPUTIMAGEWITHINFO
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

InputImageBytes = _reflection.GeneratedProtocolMessageType('InputImageBytes', (_message.Message,), {
  'DESCRIPTOR' : _INPUTIMAGEBYTES,
  '__module__' : 'hair_info_pb2'
  # @@protoc_insertion_point(class_scope:maum.pa.hair.info.InputImageBytes)
  })
_sym_db.RegisterMessage(InputImageBytes)

OutputImageWithInfo = _reflection.GeneratedProtocolMessageType('OutputImageWithInfo', (_message.Message,), {
  'DESCRIPTOR' : _OUTPUTIMAGEWITHINFO,
  '__module__' : 'hair_info_pb2'
  # @@protoc_insertion_point(class_scope:maum.pa.hair.info.OutputImageWithInfo)
  })
_sym_db.RegisterMessage(OutputImageWithInfo)


DESCRIPTOR._options = None

_HAIRINFO = _descriptor.ServiceDescriptor(
  name='HairInfo',
  full_name='maum.pa.hair.info.HairInfo',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=124,
  serialized_end=229,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetHairInfo',
    full_name='maum.pa.hair.info.HairInfo.GetHairInfo',
    index=0,
    containing_service=None,
    input_type=_INPUTIMAGEBYTES,
    output_type=_OUTPUTIMAGEWITHINFO,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_HAIRINFO)

DESCRIPTOR.services_by_name['HairInfo'] = _HAIRINFO

# @@protoc_insertion_point(module_scope)
