#-*-coding: utf-8

import torch
import torch.nn as nn
from torchvision import datasets, models, transforms
from torch.nn.functional import interpolate
import torch.nn.functional as F
import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from preprocess_for_cam import cam_preocess
from net.MobileNetV2_unet import MobileNetV2_unet
import time


deep_model_dir = './model/IOU0.7733821534051539_checkpoint5.pth'
test_model_dir = './model/실험용/신모델/valid_accuracy[0.9657143]_train_accuracy[0.99997956]_checkpoint184.pth'
#test_model_dir = './model/실험용/신모델/valid_accuracy[0.9457143]_train_accuracy[0.9999387]_checkpoint138.pth'


# FONT
location = (20, 40)
font = cv2.FONT_HERSHEY_TRIPLEX
fontScale = 1
green = (0, 255, 0)
thickness = 2

def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean  # 정규화를 해제
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(5)

def test():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    model = models.resnet18(pretrained=False)
    in_features = model.fc.in_features
    model.fc = nn.Linear(in_features=in_features, out_features=5)
    model = model.to(device)
    model.load_state_dict(torch.load(test_model_dir))
    model.eval()
    print(model)

    deep_net = MobileNetV2_unet(None)
    deep_net = deep_net.to(device)
    deep_net.load_state_dict(torch.load(deep_model_dir))
    print("model loaded from {}".format(deep_model_dir))
    deep_net.eval()


    transform_seg_data = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
       # transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    hair_color_transforms = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])
    cap = cv2.VideoCapture(0)
    while 1:
        ret, frame = cap.read()
        image = frame.copy()
        converted_deep_input_image = cam_preocess(image) # 중요
        if isinstance(converted_deep_input_image, int): # detection or landmark 감지 실패
            cv2.imshow("input", frame)
            cv2.waitKey(1)
            continue

        seg_input = converted_deep_input_image.copy()

        seg_input = cv2.cvtColor(seg_input, cv2.COLOR_BGR2RGB)
        seg_input = Image.fromarray(seg_input)
        seg_input = transform_seg_data(seg_input)
        seg_input = torch.unsqueeze(seg_input, 0)
        seg_input = seg_input.to(device)

        seg_result = deep_net(seg_input)
        seg_result = interpolate(seg_result, scale_factor=2, mode='bilinear', align_corners=False)
        seg_result[seg_result > 0.5] = 255
        seg_result[seg_result <= 0.5] = 0
        np_seg_result = seg_result.cpu().detach().numpy()

        np_seg_result = np.transpose(np_seg_result, (2, 3, 1, 0))
        np_seg_result = np_seg_result[:, :, 0, 0]

        image_original_for_hair_color = converted_deep_input_image.copy()
        image_original_for_hair_color[np_seg_result != 255] = 0

        hair_color_deep_input = cv2.cvtColor(image_original_for_hair_color, cv2.COLOR_BGR2RGB)
        hair_color_deep_input = Image.fromarray(hair_color_deep_input)
        hair_color_deep_input = hair_color_transforms(hair_color_deep_input)
        hair_color_deep_input = torch.unsqueeze(hair_color_deep_input, 0)
        hair_color_deep_input = hair_color_deep_input.to(device)

        result_str = 'None'
        with torch.set_grad_enabled(False):
            start = time.time()
            hair_color_result = model(hair_color_deep_input)
            print(hair_color_result)
            print("time :", time.time() - start)  # 현재시각 - 시작시간 = 실행 시간
            sm_color_result = F.log_softmax(hair_color_result, dim=1)
            print(sm_color_result)
            _, preds = torch.max(sm_color_result, 1)
            print(preds)
            final_result = preds.cpu().detach().numpy()

            if final_result == 0:
                result_str = "Black Hair"
            elif final_result == 1:
                result_str = "Blond Hair"
            elif final_result == 2:
                result_str = "Red Hair"
            elif final_result == 3:
                result_str = "Pink Hair"
            elif final_result == 4:
                result_str = "Brown Hair"

        cv2.putText(frame, result_str, location, font, fontScale, green, thickness);
        cv2.imshow("input", frame)
        cv2.imshow("seg_input", converted_deep_input_image)
        cv2.imshow('seg_result', image_original_for_hair_color)




        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        #fold = 'C:/Users/ParkSeongWoo/Desktop/color/4/'
        # store_name = fold + 'test_image' + str(test_count) +'.png'
        # cv2.imwrite(store_name, mask_border)
        # test_count = test_count + 1
        # time.sleep(0.5)


def prove():
    name = 'test'
    test_root = 'E:/hair_dataset/hair_color/about_color/new_construct/non_aug'
    data_transforms = {
        name: transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
    }

    path = {x: os.path.join(os.path.dirname(os.path.abspath(__file__)), test_root, x)
            for x in [name]}

    image_datasets = {x: datasets.ImageFolder(path[x],
                                              data_transforms[x])
                      for x in [name]}
    batch_size = 1
    dataloaders = {name: torch.utils.data.DataLoader(image_datasets[name], batch_size=batch_size,
                                                        shuffle=False, num_workers=0)}

    dataset_sizes = {x: len(image_datasets[x]) for x in [name]}
    print("dataset size: ", dataset_sizes[name])

    class_names = image_datasets[name].classes
    print("class num: ", class_names)
    # class_names = ['0', '1', '2', '3', '4', '5']

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    model = models.resnet50(pretrained=False)
    in_features = model.fc.in_features
    model.fc = nn.Linear(in_features=in_features, out_features=5)
    model = model.to(device)

    model.load_state_dict(torch.load(test_model_dir))
    print(model)
    model.eval()
    count = 0

    for input, label in dataloaders[name]:
        inputz = torch.squeeze(input)
        input = input.to(device)
        #input = tensor_image
        label = label.to(device)


        with torch.set_grad_enabled(False):
            start = time.time()  # 시작 시간 저장
            outputs = model(input)
            print("time :", time.time() - start)  # 현재시각 - 시작시간 = 실행 시간
            outputs_result = F.log_softmax(outputs, dim=1)
            _, preds = torch.max(outputs_result, 1)
            count += torch.sum(preds == label.data)
            if preds != label.data:
                imshow(inputz, 'zz')
                cv2.waitKey(1)


    print("맞춘 수: {0}/{1}".format(count, dataset_sizes[name]))

if __name__ == '__main__':
    test()
