import os
from hair_color_custom_dataset import HairInfoDataset
import torch
import torchvision
import torchvision.models as models
import torch.nn as nn
from torch.optim import Adam
import numpy as np
import matplotlib.pyplot as plt
from torch.optim import lr_scheduler

image_train_dir = './hair_dataset/aug/train/input_color_mask/'
image_valid_dir = './hair_dataset/aug/valid/input_color_mask/'


txt_train_dir = './hair_dataset/aug/train/label/'
txt_valid_dir = './hair_dataset/aug/valid/label/'

dir_checkpoint = './checkpoint/aug_resnet50/'

def train():

    input_id_divided_train = [f[:-4] for f in os.listdir(image_train_dir)]
    input_id_divided_valid = [f[:-4] for f in os.listdir(image_valid_dir)]

    train_ready = {'train': input_id_divided_train[:]}
    valid_ready = {'valid': input_id_divided_valid[:]}
    face_dataset_train = HairInfoDataset(train_ready['train'], image_train_dir, txt_train_dir)
    face_dataset_valid = HairInfoDataset(valid_ready['valid'], image_valid_dir, txt_valid_dir)

    _batch_size = 128

    face_loader_train = torch.utils.data.DataLoader(face_dataset_train, batch_size=_batch_size, shuffle=True, num_workers=1)
    face_loader_valid = torch.utils.data.DataLoader(face_dataset_valid, batch_size=_batch_size, shuffle=False, num_workers=1)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    model = models.resnet50(pretrained=False)
    in_features = model.fc.in_features
    model.fc = nn.Linear(in_features=in_features, out_features=7).cuda()

    if 1:
        model = nn.DataParallel(model)
    model = model.to(device)
    print(model)

    learning_rate = 0.001
    optimizer = Adam(model.parameters(), lr=learning_rate, weight_decay=1e-5)  # lr =0.001
    criterion = nn.CrossEntropyLoss()
    epochs = 400
    train_loss = np.zeros((epochs, 1), dtype=np.float32)
    train_acc = np.zeros((epochs, 1), dtype=np.float32)
    valid_loss = np.zeros((epochs, 1), dtype=np.float32)
    valid_acc = np.zeros((epochs, 1), dtype=np.float32)

    folder_name = dir_checkpoint + 'batch_size{}_lr{}/'.format(_batch_size, learning_rate)

    if not os.path.exists(folder_name):
        os.mkdir(folder_name)  # checkpoint 저장 폴더 생성

    text = open(folder_name + "result.txt", 'w')
    text.close()

    exp_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.9)
    for epoch in range(epochs):
        model.train()
        text = open(folder_name + "result.txt", 'a')
        running_loss = 0.0

        for i, image in enumerate(face_loader_train, 0):
            input = image['image']
            original_label = image['label']
            original_label = torch.squeeze(original_label)
            if 1:
                input = input.to(device)
                original_label = original_label.to(device, dtype=torch.int64)

            optimizer.zero_grad()
            with torch.set_grad_enabled(True):
                outputs = model(input)
                _, preds = torch.max(outputs.data, 1)
                loss = criterion(outputs, original_label)
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                cur_batch_size = input.size(0)
                running_loss += cur_batch_size

                train_loss[epoch] += float(loss.item() * cur_batch_size)

                k = 0
                k += torch.sum(preds == original_label).squeeze()
                train_acc[epoch] += float(k)

                print('{0:.4f} --- loss: {1:.6f}'.format((i + 1) * _batch_size / len(face_loader_train.dataset),
                                                         loss.item()))

        train_loss[epoch] = float(train_loss[epoch] / running_loss)
        train_acc[epoch] = float(train_acc[epoch] / running_loss)
        exp_lr_scheduler.step()

        print('Epoch [%2d/%2d]: Loss: %1.8f\tAccuracy: %1.8f'% (epoch + 1, epochs, train_loss[epoch], train_acc[epoch]))
        model.eval()
        val_running_loss = 0.0
        for i, image in enumerate(face_loader_valid, 0):
            input = image['image']
            original_label = image['label']
            if 1:
                input = input.to(device)
                original_label = original_label.to(device, dtype=torch.int64)

            with torch.set_grad_enabled(False):
                outputs = model(input)
                _, preds = torch.max(outputs.data, 1)
                loss = criterion(outputs, original_label)
                cur_batch_size = input.size(0)
                val_running_loss += cur_batch_size
                valid_loss[epoch] += float(loss.item() * cur_batch_size)

                k = 0
                k += torch.sum(preds == original_label).squeeze()
                valid_acc[epoch] += float(k)


                print('{0:.4f} --- loss: {1:.6f}'.format((i + 1) * _batch_size / len(face_loader_valid.dataset),
                                                         loss.item()))

        valid_loss[epoch] = float(valid_loss[epoch] / val_running_loss)
        valid_acc[epoch] = float(valid_acc[epoch] / val_running_loss)

        print("epoch:%d, valid loss:%f, valid accuracy:%f \n" % (epoch + 1, valid_loss[epoch],
                                                                 valid_acc[epoch],
                                                                 ))

        store_name = 'valid_loss{}_train_loss{}_checkpoint{}.pth'.format(valid_loss[epoch], train_loss[epoch], epoch + 1)
        torch.save(model.state_dict(), folder_name + store_name)
        print('Checkpoint {} saved !'.format(epoch + 1))

        _x = range(0, epoch + 1)
        train_y = train_loss[:epoch + 1]
        valid_y = valid_loss[:epoch + 1]

        plt.figure()
        plt.xlabel('checkpoint')
        plt.ylabel('loss')
        plt.title('batch_size{}_lr{}'.format(_batch_size, learning_rate))
        plt.plot(_x, train_y, '-r', label='train loss')
        plt.plot(_x, valid_y, '-b', label='valid loss')
        plt.legend(loc='upper left')

        plt.savefig(folder_name + "result.png")

        txt_contents = ("epoch:%d, train_data:%f, valid_data:%f \n" % (epoch + 1,
                                                                train_loss[epoch],
                                                                valid_loss[epoch]))
        text.write(txt_contents)
        text2 = open(folder_name + str(epoch + 1) +".txt" , 'w')

        data = "헤어 컬러는 [train accuracy: %f, valid_accuracy: %f 입니다]\n" %(train_acc[epoch], valid_acc[epoch])
        text2.write(data)
        text2.close()


    text.close()

if __name__ == '__main__':
    print('훈련을 시작합니다.....')
    train()