import os
import cv2
import numpy as np
from scipy import spatial
import torch
import torchvision
from torchvision import transforms
from . preprocessing import faceAlignment
from PIL import Image
from torch.autograd import Variable
import torch.nn.functional as F

from torchvision.models import resnet50
from . light_cnn import LightCNN_FV_Net, LightCNN_GENDER_Net, LightCNN_AGE_Net
import landmark.hopenet

class ETRIFace:
    def __init__(self):
        # class init
        self.rt = [-1, -1, -1, -1]
        self.fAge = -1.
        self.fGender = -1.
        self.sID = ""
        self.fvScore = -1.
        self.ptLE = [-1, -1]
        self.ptRE = [-1, -1]
        self.ptLM = [-1, -1]
        self.ptRM = [-1, -1]
        self.ptN = [-1, -1]
        self.ptLED = [-1, -1]
        self.ptLED = [-1, -1]

        self.fYaw = -1.
        self.fPitch = -1.
        self.fRoll = -1.



def ETRI_Initialization():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    FD_Net = cv2.dnn.readNetFromCaffe("./landmark/models/opencv_ssd.prototxt", "./landmark/models/opencv_ssd.caffemodel")

    FV_Net = LightCNN_FV_Net(num_classes=79994)
    GENDER_Net = LightCNN_GENDER_Net(num_classes=2)
    AGE_Net = LightCNN_AGE_Net(num_classes = 80)

    FV_Net = torch.nn.DataParallel(FV_Net).to(device)
    GENDER_Net = torch.nn.DataParallel(GENDER_Net).to(device)
    AGE_Net = torch.nn.DataParallel(AGE_Net).to(device)

    FV_Net.load_state_dict(torch.load("./landmark/models/ETRI_FV.pth.tar", map_location=device)['state_dict'])
    GENDER_Net.load_state_dict(torch.load("./landmark/models/ETRI_GENDER.pth.tar", map_location=device)['state_dict'])
    AGE_Net.load_state_dict(torch.load("./landmark/models/ETRI_AGE.pth.tar", map_location=device)['state_dict'])

    Landmark_Net = resnet50()
    Landmark_Net.fc = torch.nn.Linear(2048, 136, bias=True)

    Landmark_Net = torch.nn.DataParallel(Landmark_Net).to(device)
    Landmark_Net.load_state_dict(torch.load("./landmark/models/ETRI_LANDMARK.pth.tar", map_location=device)['state_dict'])

    HeadPose_Net = landmark.hopenet.Hopenet(torchvision.models.resnet.Bottleneck, [3, 4, 6, 3], 66)
    HeadPose_Net = HeadPose_Net.to(device)
    HeadPose_Net.load_state_dict(torch.load("./landmark/models/ETRI_HEAD_POSE.pth.tar", map_location=device))


    FV_Net.eval()
    GENDER_Net.eval()
    AGE_Net.eval()
    Landmark_Net.eval()
    HeadPose_Net.eval()

    return FD_Net, FV_Net, GENDER_Net, AGE_Net, Landmark_Net, HeadPose_Net


def ETRI_Face_Detection(FD_Net, cvImg, list_ETRIFace):
    del list_ETRIFace[:]
    img = cvImg.copy()
    (h, w) = img.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))

    FD_Net.setInput(blob)
    detections = FD_Net.forward()

    for i in range(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence > 0.5:
            # compute the (x, y)-coordinates of the bounding box for the
            # object
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            ef = ETRIFace()
            difX = endX - startX
            difY = endY - startY
            if difX > difY:
                offset = int((difX - difY)/2)
                new_startY = max(startY-offset, 0)
                new_endY = min(endY+offset, h-1)
                ef.rt = [startX, new_startY, endX, new_endY]
            else:
                offset = int((difY - difX) / 2)
                new_startX = max(startX - offset, 0)
                new_endX = min(endX + offset, w-1)
                ef.rt = [new_startX, startY, new_endX, endY]

            list_ETRIFace.append(ef)

    return len(list_ETRIFace)

def ETRI_Landmark_Detection(Landmark_Net, cvImg, list_ETRIFace, nIndex):
    img = cvImg[list_ETRIFace[nIndex].rt[1]:list_ETRIFace[nIndex].rt[3], \
          list_ETRIFace[nIndex].rt[0]:list_ETRIFace[nIndex].rt[2]].copy()

    offsetX = list_ETRIFace[nIndex].rt[0]
    offsetY = list_ETRIFace[nIndex].rt[1]


    fRate = float(img.shape[1])/224.

    # img_resize = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    try:
        img_resize = cv2.resize(img, (224,224))
    except Exception as e:
        print(str(e))
        print("Too Close !!")
        return img

    img_trim_in = img_resize / 255.
    img_trim_in = np.transpose(img_trim_in, axes=[2, 0, 1])
    img_trim_in = np.array(img_trim_in, dtype=np.float32)
    img_trim_in = torch.from_numpy(img_trim_in)
    img_trim_in = torch.unsqueeze(img_trim_in, 0)
    output = Landmark_Net(img_trim_in)
    output_cpu = output.cpu()
    output_np = output_cpu.detach().numpy().squeeze()
    output_np = output_np * 112
    output_np = output_np + 112
    output_np = output_np * fRate

    for ii in range(68):
        output_np[ii*2+0] = output_np[ii*2+0] + offsetX
        output_np[ii * 2 + 1] = output_np[ii*2+1] + offsetY

    leX = leY = reX = reY = lmX = lmY = rmX = rmY = nX = nY = 0
    for ii in range(36,42,1):
        leX = leX + output_np[ii*2+0]
        leY = leY + output_np[ii * 2 + 1]

    for ii in range(42,48,1):
        reX = reX + output_np[ii * 2 + 0]
        reY = reY + output_np[ii * 2 + 1]

    list_ETRIFace[nIndex].ptLE = [int(leX/6), int(leY/6)]
    list_ETRIFace[nIndex].ptRE = [int(reX/6), int(reY/6)]
    list_ETRIFace[nIndex].ptLM = [int(output_np[48*2+0]), int(output_np[48*2+1])]
    list_ETRIFace[nIndex].ptRM = [int(output_np[54 * 2 + 0]), int(output_np[54 * 2 + 1])]
    list_ETRIFace[nIndex].ptLM = [int(output_np[30*2+0]), int(output_np[30*2+1])]

    return output_np


transformations_lightcnn = transforms.Compose(
    [transforms.Grayscale(),
     transforms.CenterCrop(128),
     transforms.ToTensor()]
)
transformations_hopenet = transforms.Compose(
    [transforms.Scale(224),
     transforms.CenterCrop(224), transforms.ToTensor(),
     transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])]
)

def list2SoftList(srcList):
    tmpList = srcList.copy()

    fSum = 0.

    for ii in range(len(srcList)):
        fExp = np.exp(srcList[ii])
        fSum = fSum + fExp
        tmpList[ii] = fExp
    for ii in range(len(srcList)):
        srcList[ii] = tmpList[ii] / fSum;

    return srcList

def ETRI_Gender_Classification(Gender_Net, cvImg, list_ETRIFace, nIndex):
    if list_ETRIFace[nIndex].ptLE == [-1, -1]:
        return -1

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    img = cvImg.copy()

    ROIImg = faceAlignment(img, list_ETRIFace[nIndex].ptLE, list_ETRIFace[nIndex].ptRE
                           , list_ETRIFace[nIndex].ptLM, list_ETRIFace[nIndex].ptLM)

    PILROI = Image.fromarray(ROIImg)

    transformedImg = transformations_lightcnn(PILROI)
    transformedImg = torch.unsqueeze(transformedImg, 0)
    transformedImg = transformedImg.to(device)
    output_gender, _ = Gender_Net(transformedImg)
    output_cpu = output_gender.cpu().detach().numpy().squeeze()

    output = list2SoftList(output_cpu)
    if output[1] > 0.5:
        list_ETRIFace[nIndex].fGender = 0
    else:
        list_ETRIFace[nIndex].fGender = 1

    return list_ETRIFace[nIndex].fGender


def ETRI_Age_Estimation(Age_Net, cvImg, list_ETRIFace, nIndex):
    if list_ETRIFace[nIndex].ptLE == [-1, -1]:
        return -1

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    img = cvImg.copy()

    ROIImg = faceAlignment(img, list_ETRIFace[nIndex].ptLE, list_ETRIFace[nIndex].ptRE
                           , list_ETRIFace[nIndex].ptLM, list_ETRIFace[nIndex].ptLM)

    PILROI = Image.fromarray(ROIImg)

    transformedImg = transformations_lightcnn(PILROI)
    transformedImg = torch.unsqueeze(transformedImg, 0)
    transformedImg = transformedImg.to(device)
    output_age, _ = Age_Net(transformedImg)
    output_cpu = output_age.cpu().detach().numpy().squeeze()

    output = list2SoftList(output_cpu)

    fEAge = 0.
    for ii in range(80):
        fEAge = fEAge + (output[ii] * (ii+1))

    list_ETRIFace[nIndex].fAge = fEAge

    return fEAge


def ETRI_HeadPose_Estimation(HeadPose_Net, cvImg, list_ETRIFace, nIndex):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    oImg = cvImg[list_ETRIFace[nIndex].rt[1]:list_ETRIFace[nIndex].rt[3], \
          list_ETRIFace[nIndex].rt[0]:list_ETRIFace[nIndex].rt[2]].copy()

    idx_tensor = [idx for idx in range(66)]
    idx_tensor = torch.FloatTensor(idx_tensor).to(device)

    PILImg = Image.fromarray(oImg)

    img = transformations_hopenet(PILImg)
    img_shape = img.size()
    img = img.view(1, img_shape[0], img_shape[1], img_shape[2])
    img = Variable(img).to(device)

    yaw, pitch, roll = HeadPose_Net(img)

    yaw_predicted = F.softmax(yaw)
    pitch_predicted = F.softmax(pitch)
    roll_predicted = F.softmax(roll)

    yaw_predicted = torch.sum(yaw_predicted.data[0] * idx_tensor) * 3 - 99
    pitch_predicted = torch.sum(pitch_predicted.data[0] * idx_tensor) * 3 - 99
    roll_predicted = torch.sum(roll_predicted.data[0] * idx_tensor) * 3 - 99

    list_ETRIFace[nIndex].fYaw = yaw_predicted
    list_ETRIFace[nIndex].fPitch = pitch_predicted
    list_ETRIFace[nIndex].fRoll = roll_predicted

    return (yaw_predicted, pitch_predicted, roll_predicted)


def ETRI_Face_Enrollment(FV_Net, cvImg, list_ETRIFace, nIndex, sID, nAngleIndex):
    if list_ETRIFace[nIndex].ptLE == [-1, -1]:
        return -1

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    img = cvImg.copy()

    ROIImg = faceAlignment(img, list_ETRIFace[nIndex].ptLE, list_ETRIFace[nIndex].ptRE
                           , list_ETRIFace[nIndex].ptLM, list_ETRIFace[nIndex].ptLM)

    PILROI = Image.fromarray(ROIImg)

    transformedImg = transformations_lightcnn(PILROI)
    transformedImg = torch.unsqueeze(transformedImg, 0)
    transformedImg = transformedImg.to(device)
    _, Tfeature = FV_Net(transformedImg)
    feature = Tfeature.data[0].cpu().detach().numpy().squeeze()

    saveID = "./landmark/EnrolledData/{saveID}_{saveAngle:02d}.npy".format(saveID = sID, saveAngle = nAngleIndex)
    np.save(saveID, feature)


def ETRI_Face_Verification(FV_Net, cvImg, list_ETRIFace, nIndex):
    if list_ETRIFace[nIndex].ptLE == [-1, -1]:
        return -1

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    img = cvImg.copy()

    ROIImg = faceAlignment(img, list_ETRIFace[nIndex].ptLE, list_ETRIFace[nIndex].ptRE
                           , list_ETRIFace[nIndex].ptLM, list_ETRIFace[nIndex].ptLM)

    PILROI = Image.fromarray(ROIImg)

    transformedImg = transformations_lightcnn(PILROI)
    transformedImg = torch.unsqueeze(transformedImg, 0)
    transformedImg = transformedImg.to(device)
    _, Tfeature = FV_Net(transformedImg)
    feature = Tfeature.data[0].cpu().detach().numpy().squeeze()

    sMaxFileName, fMaxSim = CompareFeature(feature)

    list_ETRIFace[nIndex].sID = sMaxFileName.split('_')[0]
    list_ETRIFace[nIndex].fvScore = fMaxSim


def CompareFeature(feature):
    fMaxSimilarity = -1.
    sMaxFileName = ""

    dirName = "./landmark/EnrolledData/"
    filenames = os.listdir(dirName)
    for filename in filenames:
        full_filename = os.path.join(dirName, filename)
        load_feature = np.load(full_filename)

        fSim = 1 - spatial.distance.cosine(feature, load_feature)

        if fSim > fMaxSimilarity:
            fMaxSimilarity = fSim
            sMaxFileName = filename


    return sMaxFileName, fMaxSimilarity










































