import numpy as np
import cv2
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
import torch.backends.cudnn as cudnn
import torchvision
import torch.nn.functional as F
from PIL import Image


import landmark.hopenet


if __name__ == '__main__':
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    model = landmark.hopenet.Hopenet(torchvision.models.resnet.Bottleneck, [3, 4, 6, 3], 66)
    saved_state_dict = torch.load("./landmark/hopenet_robust_alpha1.pkl")
    model.load_state_dict(saved_state_dict)

    model.to(device)

    transformations = transforms.Compose([transforms.Scale(224),
                                          transforms.CenterCrop(224), transforms.ToTensor(),
                                          transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])



    model.eval()

    idx_tensor = [idx for idx in range(66)]
    idx_tensor = torch.FloatTensor(idx_tensor).to(device)

    vc = cv2.VideoCapture(0)
    while (True):
        _, frame = vc.read()
        frame = cv2.flip(frame, 1)

        cv2_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        img = cv2_frame[200:300, 200:300]
        img = Image.fromarray(img)

        # Transform
        img = transformations(img)
        img_shape = img.size()
        img = img.view(1, img_shape[0], img_shape[1], img_shape[2])
        img = Variable(img).to(device)

        yaw, pitch, roll = model(img)

        yaw_predicted = F.softmax(yaw)
        pitch_predicted = F.softmax(pitch)
        roll_predicted = F.softmax(roll)

        yaw_predicted = torch.sum(yaw_predicted.data[0] * idx_tensor) * 3 - 99
        pitch_predicted = torch.sum(pitch_predicted.data[0] * idx_tensor) * 3 - 99
        roll_predicted = torch.sum(roll_predicted.data[0] * idx_tensor) * 3 - 99

        sout = ' %f %f %f\n' % (yaw_predicted, pitch_predicted, roll_predicted)
        print(sout)

        cv2.rectangle(frame, (200,200), (300,300), (0,255,0))
        cv2.imshow("TT", frame)
        cv2.waitKey(1)


