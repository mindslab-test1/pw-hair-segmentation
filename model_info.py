from torchsummary import summary
import torchvision.models as models
from net.MobileNetV2_unet import MobileNetV2_unet
import torch
import torch.nn as nn

test_model_dir = "./model/IOU0.7733821534051539_checkpoint5.pth"
color_test_model_dir = './model/실험용/신모델/valid_accuracy[0.9657143]_train_accuracy[0.99997956]_checkpoint184.pth'
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

net = MobileNetV2_unet(None)
net = net.to(device)
net.load_state_dict(torch.load(test_model_dir))
print("model loaded from {}".format(test_model_dir))
net.eval()

print(summary(net, (3, 224, 224)))
#print(sum([param.nelement() for param in net.parameters()]))

model = models.resnet18(pretrained=False)
in_features = model.fc.in_features
model.fc = nn.Linear(in_features=in_features, out_features=5)
model = model.to(device)
model.load_state_dict(torch.load(color_test_model_dir))
model.eval()

#print(sum([param.nelement() for param in model.parameters()]))

print(summary(model, (3, 224, 224)))

