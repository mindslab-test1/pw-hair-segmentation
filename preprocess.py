import face_recognition
import os
import cv2
import random
from PIL import Image
import copy
import numpy as np
import cv2
import landmark.ETRI_Face_Information as EFI

FD_Net, FV_Net, GENDER_Net, AGE_Net, Landmark_Net, HeadPose_Net = EFI.ETRI_Initialization()

read_txt_dir= "./dataset/split/feature_txt_CELELBA/"
txt_dir = "./right_dataset/total/label/"


def refine_bald_data():
    input_dir = 'C:/Users/ParkSeongWoo/Desktop/hairhair/bald/'
    store_dir = 'C:/Users/ParkSeongWoo/Desktop/hairhair/preprocess/'
    mask_dir = 'C:/Users/ParkSeongWoo/Desktop/hairhair/mask/'
    if not os.path.exists(input_dir):
        raise ValueError('입력 이미지 경로가 올바르지 않습니다.(%s)' % input_dir)

    list_id = [f[:-4] for f in os.listdir(input_dir)]

    detect_list = []
    landmark_list = []

    for ids in list_id:
        detect_face_list = []
        print("%s를 불러옵니다....." % ids)
        _input_dir = input_dir + ids + '.jpg'
        image = cv2.imread(_input_dir, cv2.IMREAD_COLOR)
        im_height, im_width, im_channel = image.shape
        EFI.ETRI_Face_Detection(FD_Net, image, detect_face_list)

        if len(detect_face_list) == 0:
            detect_list.append(ids)
            print("Detecting failed")
            return 0

        landmark = EFI.ETRI_Landmark_Detection(Landmark_Net, image, detect_face_list, 0)

        if len(landmark) == 0:
            landmark_list.append(ids)
            print("LandMark Failed")
            return 0

        left_eye = [landmark[39 * 2 + 0], landmark[39 * 2 + 1]]
        right_eye = [landmark[42 * 2 + 0], landmark[42 * 2 + 1]]
        nose_bridge = [landmark[30 * 2 + 0], landmark[30 * 2 + 1]]

        (left_eye_x, left_eye_y) = left_eye
        (right_eye_x, right_eye_y) = right_eye
        (c_x, c_y) = nose_bridge

        height_value = right_eye_x - left_eye_x
        per_height_bottom_value = height_value * 13

        rect_upper = (int(c_x - per_height_bottom_value / 2), int(c_y - per_height_bottom_value / 2))
        rect_bottom = (int(c_x + per_height_bottom_value / 2), int(c_y + per_height_bottom_value / 2))

        ux, uy = rect_upper
        bx, by = rect_bottom

        if by > im_height:
            target = by - im_height
            image = cv2.copyMakeBorder(image, top=0, bottom=target, left=0, right=0,
                                       borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255))
        if uy < 0:
            target = abs(uy)
            image = cv2.copyMakeBorder(image, top=target, bottom=0, left=0, right=0,
                                       borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255))
            by = by + target
            uy = 0

        if ux < 0:
            target = abs(ux)
            image = cv2.copyMakeBorder(image, top=0, bottom=0, left=target, right=0,
                                       borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255))
            bx = bx + target
            ux = 0

        if bx > im_width:
            target = bx - im_width
            image = cv2.copyMakeBorder(image, top=0, bottom=0, left=0, right=target,
                                       borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255))

        crop_img = image[uy:by, ux:bx]
        crop_height, crop_width, crop_channel = crop_img.shape

        mask_border = crop_img

        if crop_height < 224 and crop_width < 224:
            complete = cv2.resize(mask_border, dsize=(224, 224), interpolation=cv2.INTER_CUBIC)
        else:
            complete = cv2.resize(mask_border, dsize=(224, 224), interpolation=cv2.INTER_AREA)

        mask = np.zeros((224, 224))

        save_dir = store_dir + 'bold' + ids + '_no_flip' '.png'
        save_mask_dir = mask_dir + 'bold' + ids + '_no_flip_mask.png'


        cv2.imwrite(save_dir, complete)
        cv2.imwrite(save_mask_dir, mask)
        cv2.imshow('gogo', complete)
        cv2.waitKey(1)
        print('wait')

    print(detect_list)
    print(landmark_list)


def train_valid():
    divide_dir = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_seg/non_aug/total/input_image/'


    train_dir = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_seg/non_aug/train/input_image/'
    valid_dir = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_seg/non_aug/valid/input_image/'

    id_dir = [f[:-4] for f in os.listdir(divide_dir)]
    random.shuffle(id_dir)
    train_data = id_dir[:6410]
    valid_data = id_dir[6410:]

    for ids in train_data:
        train_loc = divide_dir + ids + ".png"
        image = cv2.imread(train_loc, 1)
        store_name = train_dir + ids + ".png"
        cv2.imwrite(store_name, image)

    for ids in valid_data:
        train_loc = divide_dir + ids + ".png"
        image = cv2.imread(train_loc, 1)
        store_name = valid_dir + ids + ".png"
        cv2.imwrite(store_name, image)




def randomize_image():
    train_dir = "E:/hair_dataset/hair_bold/non_aug/original/1/"
    store_dir = 'E:/hair_dataset/hair_bold/non_aug/train/1/'
    sub_store_dir = 'E:/hair_dataset/hair_bold/non_aug/valid/1/'

    id_dir = [f[:-4] for f in os.listdir(train_dir)]

    random.shuffle(id_dir)
    main_dir = id_dir[:167]
    sub_dir = id_dir[167:]

    for ids in main_dir:
        train_loc = train_dir + ids + ".jpg"
        image = cv2.imread(train_loc, 1)
        image[image > 250] = 255
        store_name = store_dir + ids + ".png"
        cv2.imwrite(store_name, image)

    for ids in sub_dir:
        train_loc = train_dir + ids + ".jpg"
        image = cv2.imread(train_loc, 1)
        image[image > 250] = 255
        store_name = sub_store_dir + ids + ".png"
        cv2.imwrite(store_name, image)


def put_image_per_color():
    train_dir = "E:/hair_dataset/non_aug/total/input_image/"
    train_dir_txt = "E:/hair_dataset/non_aug/total/label/"
    store_dir = 'C:/Users/ParkSeongWoo/Desktop/분석/0'


    id_dir = [f[:-4] for f in os.listdir(train_dir)]
    print("데이터 갯수는 총 %d개 입니다." % len(id_dir))
    count = 0
    for ids in id_dir:
        print("%s를 불러옵니다....." % ids)
        txt_ids = ids
        train_loc = train_dir + ids + ".jpg"
        txt_loc = train_dir_txt + txt_ids + ".txt"

        t = open(txt_loc, 'r')
        lines = t.readlines()
        token = []
        count = 0
        for i in lines:
            token.append(i[-2])
            break

        txt_result = token.copy()
        txt_result = list(map(int, txt_result))

        if txt_result == [3]:
            image = cv2.imread(train_loc, 1)
            store_name = store_dir + ids + ".jpg"
            cv2.imwrite(store_name, image)
            count = count + 1
            print("%s가 처리됩니다... %d/%d 진행되었습니다" % (ids, count, len(id_dir)))
        else:
            continue


def save_txt():
    if not os.path.exists(read_txt_dir) or not os.path.isdir(read_txt_dir):
        raise ValueError('텍스트 저장 경로가 올바르지 않습니다.(%s)' % read_txt_dir)

    if not os.path.exists(txt_dir) or not os.path.isdir(txt_dir):
        raise ValueError('텍스트 저장된 경로가 올바르지 않습니다.(%s)' % txt_dir)

    id_dir = [f[:-4] for f in os.listdir(read_txt_dir)]
    print("txt 갯수는 총 %d개 입니다." % len(id_dir))

    for ids in id_dir:
        print("%s를 불러옵니다....." % ids)

        txt_total_dir = read_txt_dir + ids + ".txt"

        f = open(txt_total_dir, 'r')
        lines =f.readlines()

        f1 = open(txt_dir + ids + "_flip.txt", 'w')
        f1.writelines(lines)
        f1.close()
        f2 = open(txt_dir + ids + "_flip5.txt", 'w')
        f2.writelines(lines)
        f2.close()
        f3 = open(txt_dir + ids + "_flip-5.txt", 'w')
        f3.writelines(lines)
        f3.close()
        f4 = open(txt_dir + ids + "_flip10.txt", 'w')
        f4.writelines(lines)
        f4.close()
        f5 = open(txt_dir + ids + "_flip-10.txt", 'w')
        f5.writelines(lines)
        f5.close()
        f6 = open(txt_dir + ids + "_flip15.txt", 'w')
        f6.writelines(lines)
        f6.close()
        f7 = open(txt_dir + ids + "_flip-15.txt", 'w')
        f7.writelines(lines)
        f7.close()

        f8 = open(txt_dir + ids + "_no_flip.txt", 'w')
        f8.writelines(lines)
        f8.close()
        f9 = open(txt_dir + ids + "_no_flip5.txt", 'w')
        f9.writelines(lines)
        f9.close()
        f10 = open(txt_dir + ids + "_no_flip-5.txt", 'w')
        f10.writelines(lines)
        f10.close()
        f11 = open(txt_dir + ids + "_no_flip10.txt", 'w')
        f11.writelines(lines)
        f11.close()
        f12 = open(txt_dir + ids + "_no_flip-10.txt", 'w')
        f12.writelines(lines)
        f12.close()
        f13 = open(txt_dir + ids + "_no_flip15.txt", 'w')
        f13.writelines(lines)
        f13.close()
        f14 = open(txt_dir + ids + "_no_flip-15.txt", 'w')
        f14.writelines(lines)
        f14.close()

        f.close()


dir_input = "C:/Users/ParkSeongWoo/Desktop/analysis/about_seg/non_aug/total/input_image/"
#
dir_deep_mask = "C:/Users/ParkSeongWoo/Desktop/analysis/about_curly/deep_mask_result/"
# dir_real_mask = "E:\hair_dataset/aug/total/input_mask/"
#
dir_out_mask = "C:/Users/ParkSeongWoo/Desktop/analysis/about_curly/overlap_enlarge/"
# dir_real_out_mask = "E:\hair_dataset/aug/total/input_mask_color_image/"

def save_mask_color():
    id_dir = [f[:-4] for f in os.listdir(dir_input)]
    print("데이터 갯수는 총 %d개 입니다." % len(dir_input))
    count = 0
    for ids in id_dir:
        print("%s를 불러옵니다....." % ids)
        find_train = dir_input + ids + ".png"
        image = cv2.imread(find_train, 1)

        deep_mask_full_dir = dir_deep_mask + ids + "_mask.png"
        deep_mask = cv2.imread(deep_mask_full_dir, 0)
        image[deep_mask != 255] = 0

        top = 0
        left = 0
        right = 0
        bottom = 0

        for i in range(224):
            for j in range(224):
                if deep_mask[i, j] == 255:
                    top = i
                    break
            if top != 0:
                break

        for i in range(223, -1, -1):
            for j in range(223, -1, -1):
                if deep_mask[i, j] == 255:
                    bottom = i
                    break
            if bottom !=0:
                break

        for i in range(224):
            for j in range(224):
                if deep_mask[j, i] == 255:
                    left = i
                    break
            if left != 0:
                break

        for i in range(223, -1, -1):
            for j in range(223, -1, -1):
                if deep_mask[j, i] == 255:
                    right = i
                    break
            if right !=0:
                break

        new_height = abs(top - bottom)
        new_width = abs(right - left)
        image_rect = image[top:top + new_height, left:left + new_width]

        if image_rect.size == 0:
            image_rect = image
            deep_mask_output_full_dir = dir_out_mask + ids + ".png"
            cv2.imwrite(deep_mask_output_full_dir, image_rect)
            count = count + 1
            print("%s가 처리됩니다... %d/%d 진행되었습니다" % (ids, count, len(id_dir)))
            continue

        image_rect = cv2.resize(image_rect, dsize=(224, 224), interpolation=cv2.INTER_NEAREST)

        cv2.circle(image, (left, top),4, (255, 255, 0,), -1)
        cv2.circle(image, (right, bottom),4, (0, 255, 255,), -1)
        cv2.rectangle(image, (right, bottom), (left, top), (0, 255, 0), 2)
        #cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 2)
        cv2.imshow("binary image", image)
        cv2.imshow("store image", image_rect)
        cv2.waitKey(1)
        # image_rect = image[y:y+h, left:x+w]
        # image_rect = cv2.resize(image_rect, dsize=(224, 224), interpolation=cv2.INTER_NEAREST)
        # cv2.imshow("result_rect", image_rect)

        deep_mask_output_full_dir = dir_out_mask + ids + ".png"

        cv2.imwrite(deep_mask_output_full_dir, image_rect)

        #cv2.imwrite(real_mask_output_full_dir, image2)
        count = count + 1

        print("%s가 처리됩니다... %d/%d 진행되었습니다" %(ids, count, len(id_dir)))

        # ret, threshold = cv2.threshold(deep_mask, 100, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        # img_binary = ~threshold
        # cv2.imshow("binary image", img_binary)
        # contours, hierarchy = cv2.findContours(img_binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #
        # max_area = 0
        # ci = 0
        # for i in range(len(contours)):
        #     cnt = contours[i]
        #     area = cv2.contourArea(cnt)
        #     if area > max_area:
        #         max_area = area
        #         print(max_area)
        #         ci = i
        # cnt = contours[ci]
        # image_contour = image.copy()
        #
        # cv2.drawContours(image_contour, [cnt], 0, (255, 0, 0), 1)  # blue
        # cv2.imshow("contour_image", image_contour)
        #
        # image_box = image.copy()
        #
        # x, y, w, h = cv2.boundingRect(cnt)
        # cv2.rectangle(image_box, (x, y), (x + w, y + h), (0, 255, 0), 2)
        # cv2.imshow("image_box", image_box)
        #
        # image_rect = image[y:y+h, x:x+w]
        # image_rect = cv2.resize(image_rect, dsize=(224, 224), interpolation=cv2.INTER_NEAREST)
        # cv2.imshow("result_rect", image_rect)
        #
        # cv2.waitKey(1)
        #
        # deep_mask_output_full_dir = dir_out_mask + ids + ".png"
        # #real_mask_output_full_dir = dir_real_out_mask + ids + "_color_mask.jpg"
        #
        # cv2.imwrite(deep_mask_output_full_dir, image_rect)
        #
        # #cv2.imwrite(real_mask_output_full_dir, image2)
        # count = count + 1
        #
        # print("%s가 처리됩니다... %d/%d 진행되었습니다" %(ids, count, len(id_dir)))



machine_input='E:/hair_dataset/hair_color/non_aug/valid/5/'
txt_output = 'E:/hair_dataset/hair_color/non_aug/ml_data/valid/label/'
label_value = '5'
def make_machine_txt():

    id_dir = [f[:-4] for f in os.listdir(machine_input)]
    print("이미지 갯수는 총 %d개 입니다." % len(id_dir))

    for ids in id_dir:
        print("%s를 불러옵니다....." % ids)
        f = open(txt_output + ids + ".txt", 'w')
        f.writelines(label_value)
        f.close()

def data_roate(image, width, height, angle, is_mask = 0):
    src = image.copy()
    temp_src = cv2.getRotationMatrix2D(((width - 1) / 2.0, (width - 1) / 2.0),  # 회전중심
                                       angle,  # 회전각도(양수 반시계, 음수 시계)
                                       1)  # 이미지 배율
    if not is_mask:
        rotate_src = cv2.warpAffine(src, temp_src, (width, height), borderValue=(0, 0, 0))
    else:
        rotate_src = cv2.warpAffine(src, temp_src, (width, height), borderValue=(0, 0, 0))

    rgb_border = cv2.cvtColor(rotate_src, cv2.COLOR_BGR2RGB)
    pil_border = Image.fromarray(rgb_border)

    return pil_border

def data_save(image, image_mask, ouput_dir, output_mask_dir, folder_name="", id=''):
    output_total = ouput_dir
    #output_mask_total = output_mask_dir
    image.save(output_total + id + '_' + folder_name + '.png')
    #image_mask.save(output_mask_total + id + '_' + folder_name + '_mask.png')


def make_augment():
    aug_input = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_lipstick/data/train/non_aug/2/'

    aug_output = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_lipstick/data/train/aug/2/'
    #aug_mask_output = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_seg/aug/total/input_mask/'
    id_dir = [f[:-4] for f in os.listdir(aug_input)]
    print("데이터 갯수는 총 %d개 입니다.l" % len(aug_input))
    count = 0
    for ids in id_dir:
        print("%s를 불러옵니다....." % ids)
        find_train = aug_input + ids + ".png"
        #find_mask_train = aug_input_mask + ids + "_mask.png"
        image_original = cv2.imread(find_train, 1)
        #input_mask = cv2.imread(find_mask_train)

        image_flip = cv2.flip(image_original, 1)
        #imaeg_mask_flip = cv2.flip(input_mask, 1)




        width = 224
        height = 224

        image_original_no_flip5 = data_roate(image_original, width, height, 5)
        image_original_no_flip15 = data_roate(image_original, width, height, 15)
        image_original_no_flip30 = data_roate(image_original, width, height, 30)
        image_original_no_flipm5 = data_roate(image_original, width, height, -5)
        image_original_no_flipm15 = data_roate(image_original, width, height, -15)
        image_original_no_flipm30 = data_roate(image_original, width, height, -30)

        image_original_flip5 = data_roate(image_flip, width, height, 5)
        image_original_flip15 = data_roate(image_flip, width, height, 15)
        image_original_flip30 = data_roate(image_flip, width, height, 30)
        image_original_flipm5 = data_roate(image_flip, width, height, -5)
        image_original_flipm15 = data_roate(image_flip, width, height, -15)
        image_original_flipm30 = data_roate(image_flip, width, height, -30)

        # image_mask_no_flip5 = data_roate(input_mask, width, height, 5, 1)
        # image_mask_no_flip15 = data_roate(input_mask, width, height, 15, 1)
        # image_mask_no_flip30 = data_roate(input_mask, width, height, 30, 1)
        # image_mask_no_flipm5 = data_roate(input_mask, width, height, -5, 1)
        # image_mask_no_flipm15 = data_roate(input_mask, width, height, -15, 1)
        # image_mask_no_flipm30 = data_roate(input_mask, width, height, -30, 1)
        #
        # image_mask_flip5 = data_roate(imaeg_mask_flip, width, height, 5, 1)
        # image_mask_flip15 = data_roate(imaeg_mask_flip, width, height, 15, 1)
        # image_mask_flip30 = data_roate(imaeg_mask_flip, width, height, 30, 1)
        # image_mask_flipm5 = data_roate(imaeg_mask_flip, width, height, -5, 1)
        # image_mask_flipm15 = data_roate(imaeg_mask_flip, width, height, -15, 1)
        # image_mask_flipm30 = data_roate(imaeg_mask_flip, width, height, -30, 1)


        rgb_border = cv2.cvtColor(image_original, cv2.COLOR_BGR2RGB)
        image_original = Image.fromarray(rgb_border)

        rgb_border = cv2.cvtColor(image_flip, cv2.COLOR_BGR2RGB)
        image_flip = Image.fromarray(rgb_border)

        # rgb_border = cv2.cvtColor(input_mask, cv2.COLOR_BGR2RGB)
        # input_mask = Image.fromarray(rgb_border)
        #
        # rgb_border = cv2.cvtColor(imaeg_mask_flip, cv2.COLOR_BGR2RGB)
        # imaeg_mask_flip = Image.fromarray(rgb_border)

        input_mask = []
        imaeg_mask_flip = []
        aug_mask_output = ""
        image_mask_no_flip5 = []
        image_mask_no_flip15 = []
        image_mask_no_flip30 = []
        image_mask_no_flipm5 = []
        image_mask_no_flipm15 = []
        image_mask_no_flipm30 = []
        image_mask_flip5 = []
        image_mask_flip15 = []
        image_mask_flip30 = []
        image_mask_flipm5 = []
        image_mask_flipm15 = []
        image_mask_flipm30 = []

        data_save(image_original, input_mask, aug_output, aug_mask_output, 'no_flip', ids)
        data_save(image_flip, imaeg_mask_flip, aug_output, aug_mask_output, 'flip', ids)


        data_save(image_original_no_flip5, image_mask_no_flip5, aug_output, aug_mask_output, 'no_flip5', ids)
        data_save(image_original_no_flip15, image_mask_no_flip15, aug_output, aug_mask_output, 'no_flip15', ids)
        data_save(image_original_no_flip30, image_mask_no_flip30, aug_output,  aug_mask_output, 'no_flip30', ids)
        data_save(image_original_no_flipm5, image_mask_no_flipm5, aug_output, aug_mask_output, 'no_flip-5', ids)
        data_save(image_original_no_flipm15,image_mask_no_flipm15,  aug_output, aug_mask_output, 'no_flip-15', ids)
        data_save(image_original_no_flipm30,image_mask_no_flipm30, aug_output, aug_mask_output, 'no_flip-30', ids)
        data_save(image_original_flip5, image_mask_flip5,  aug_output, aug_mask_output, 'flip5', ids)
        data_save(image_original_flip15, image_mask_flip15, aug_output, aug_mask_output, 'flip15', ids)
        data_save(image_original_flip30, image_mask_flip30, aug_output, aug_mask_output, 'flip30', ids)
        data_save(image_original_flipm5, image_mask_flipm5, aug_output, aug_mask_output, 'flip-5', ids)
        data_save(image_original_flipm15, image_mask_flipm15, aug_output, aug_mask_output,'flip-15', ids)
        data_save(image_original_flipm30, image_mask_flipm30, aug_output, aug_mask_output, 'flip-30', ids)

        count = count + 1
        print('\r>>  mask changing about size.... %s ....(%d/%d)' % (id, count, len(id_dir)))

    print('complete')



def input_label_matching():
    input_dir = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_seg/non_aug/valid/input_image/'
    find_dir = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_seg/non_aug/total/input_mask/'
    store_dir = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_seg/non_aug/valid/input_mask/'
    if not os.path.exists(input_dir):
        raise ValueError('입력 이미지 경로가 올바르지 않습니다.(%s)' % input_dir)

    list_id = [f[:-4] for f in os.listdir(input_dir)]

    for ids in list_id:
        print("%s를 불러옵니다....." % ids)
        _input_dir = find_dir + ids + '_mask.png'
        image = cv2.imread(_input_dir)
        store_name = store_dir + ids + '_mask.png'
        cv2.imwrite(store_name, image)



def store_lipstick_part():
    refer_dir = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_lipstick/lipstick_label/temp/new_0_data/'
    input_dir = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_lipstick/lipstick_label/temp/new_0_data/'
    store_dir = 'C:/Users/ParkSeongWoo/Desktop/analysis/about_lipstick/lipstick_label/temp/new_0_data_subtract/'

    #refer_dir = 'C:/Users/ParkSeongWoo/Desktop/lip/pink/2_2/'
    #input_dir = 'C:/Users/ParkSeongWoo/Desktop/lip/pink/2_2/'
    #store_dir = 'C:/Users/ParkSeongWoo/Desktop/lip/pink/2_2subtract/'
    file_ext = '.png'
    if not os.path.exists(input_dir):
        raise ValueError('입력 이미지 경로가 올바르지 않습니다.(%s)' % input_dir)

    list_id = [f[:-4] for f in os.listdir(input_dir)]
    detect_list = []
    landmark_list = []
    count = 0
    for ids in list_id:
        detect_face_list = []
        lip_area = []
        lip_non_area = []
        print("%s를 불러옵니다....." % ids)
        _input_dir = refer_dir + ids + file_ext
        ori_image = cv2.imread(_input_dir, cv2.IMREAD_COLOR)
        if ori_image is None:
            continue
        result = ori_image.copy()
        landmark_lip = ori_image.copy()

        image = face_recognition.load_image_file(_input_dir)
        face_landmarks_list = face_recognition.face_landmarks(image)

        if face_landmarks_list == []:
            print("얼굴에 포인터가 안찍혀요")
            face_landmarks_list.append(ids)
            continue
        else:
            list_landmark_value = face_landmarks_list[0]


        for i in range(12):
            l1 = list_landmark_value['bottom_lip'][i]
            cv2.circle(landmark_lip, l1, 1, (0, 255, 0), 2)
            l2 = list_landmark_value['top_lip'][i]
            cv2.circle(landmark_lip, l2, 1, (0, 255, 0), 2)


        for i in range(12):
            points1 = list_landmark_value['bottom_lip'][i]
            if i >= 7:
                lip_non_area.append(points1)
            else:
                lip_area.append(points1)

        for i in range(12):
            points1 = list_landmark_value['top_lip'][i]
            if i >= 7:
                lip_non_area.append(points1)
            else:
                lip_area.append(points1)


        np_lip_area = np.asarray(lip_area).astype(np.int32)
        lip_non_area = np.asarray(lip_non_area).astype(np.int32)

        cv2.polylines(ori_image,  [np_lip_area], 1, (250, 249, 248), 1)
        cv2.fillConvexPoly(ori_image, np_lip_area, (250, 249, 248))

        cv2.polylines(ori_image, [lip_non_area], 1, (247, 246, 245), 1)
        cv2.fillConvexPoly(ori_image, lip_non_area, (247, 246, 245))

        excluded_color = [250, 249, 248]
        indices_list = np.where(np.any(ori_image != excluded_color, axis=-1))
        result[indices_list] = [0, 0, 0]

        binary_result = result.copy()

        indices_list = np.where(np.any(result != [0, 0, 0], axis=-1))
        binary_result[indices_list] = [255, 255, 255]

        binary_result = cv2.cvtColor(binary_result, cv2.COLOR_BGR2GRAY)

        top = 0
        left = 0
        right = 0
        bottom = 0

        h, w = binary_result.shape

        for i in range(h):
            for j in range(w):
                if binary_result[i, j] == 255:
                    top = i
                    break
            if top != 0:
                break

        for i in range(h - 1, -1, -1):
            for j in range(w - 1, -1, -1):
                if binary_result[i, j] == 255:
                    bottom = i
                    break
            if bottom !=0:
                break

        for i in range(w):
            for j in range(h):
                if binary_result[j, i] == 255:
                    left = i
                    break
            if left != 0:
                break

        for i in range(w - 1, -1, -1):
            for j in range(h - 1, -1, -1):
                if binary_result[j, i] == 255:
                    right = i
                    break
            if right !=0:
                break

        new_height = abs(top - bottom)
        new_width = abs(right - left)
        image_rect = result[top:top + new_height, left:left + new_width]

        if image_rect.size == 0:
            print("%s가 처리됩니다... %d/%d 진행되었으나 0이므로 생략합니다" % (ids, count, len(_input_dir)))
            continue

        image_rect = cv2.resize(image_rect, dsize=(224, 224), interpolation=cv2.INTER_LINEAR + cv2.INTER_CUBIC)


        cv2.imshow("input", ori_image)
        cv2.imshow("landmark", landmark_lip)
        cv2.imshow("result", result)
        cv2.imshow("store image", image_rect)
        cv2.waitKey(1)

        deep_mask_output_full_dir = store_dir + ids + ".png"
        cv2.imwrite(deep_mask_output_full_dir, image_rect)
        count = count + 1
        print("%s가 처리됩니다... %d/%d 진행되었습니다" %(ids, count, len(list_id)))


        # EFI.ETRI_Face_Detection(FD_Net, image, detect_face_list)
        #
        # if len(detect_face_list) == 0:
        #     detect_list.append(ids)
        #     print("Detecting failed")
        #     return 0
        #
        # landmark = EFI.ETRI_Landmark_Detection(Landmark_Net, image, detect_face_list, 0)
        #
        # if len(landmark) == 0:
        #     landmark_list.append(ids)
        #     print("LandMark Failed")
        #     return 0
        #
        # for ii in range(68):
        #     cv2.circle(landmark_image, (landmark[ii * 2 + 0], landmark[ii * 2 + 1]), 1, (0, 0, 255), -1)
        #
        #
        # for i in range(48, 60):
        #     points = [landmark[i * 2 + 0], landmark[i * 2 + 1]]
        #     lip_area.append(points)
        #
        # for i in range(60, 68):
        #     points = [landmark[i * 2 + 0], landmark[i * 2 + 1]]
        #     lip_non_area.append(points)

        # np_lip_area = np.asarray(lip_area).astype(np.int32)
        # lip_non_area = np.asarray(lip_non_area).astype(np.int32)
        #
        # cv2.polylines(image,  [np_lip_area], 1, (100, 100, 100), 1)
        # cv2.fillConvexPoly(image, np_lip_area, (100, 100, 100))
        #
        # cv2.polylines(image, [lip_non_area], 1, (225, 225, 225), 1)
        # cv2.fillConvexPoly(image, lip_non_area, (225, 225, 225))
        #
        # excluded_color = [100, 100, 100]
        # indices_list = np.where(np.any(image != excluded_color, axis=-1))
        # result[indices_list] = [0, 0, 0]
        #
        # cv2.imshow("input", image)
        # cv2.imshow("landmark", landmark_image)
        # cv2.imshow("result", result)
        # cv2.waitKey(1)
        # print("breakpoints")









if __name__ == '__main__':
    #make_machine_txt()
    make_augment()
    #save_mask_color()
    #store_lipstick_part()
