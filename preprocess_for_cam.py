import cv2
import landmark.ETRI_Face_Information as EFI

FD_Net, FV_Net, GENDER_Net, AGE_Net, Landmark_Net, HeadPose_Net = EFI.ETRI_Initialization()


def cam_preocess(image, show=True):
    detect_face_list = []
    im_height, im_width, im_channel = image.shape
    if im_channel == 4:
        image = image[:, :, :3]

    EFI.ETRI_Face_Detection(FD_Net, image, detect_face_list)

    if len(detect_face_list) == 0:
        print("Detecting failed")
        return 0

    landmark = EFI.ETRI_Landmark_Detection(Landmark_Net, image, detect_face_list, 0)

    if len(landmark) == 0:
        print("LandMark Failed")
        return 0

    landmark_image = image.copy()
    for ii in range(68):
        cv2.circle(landmark_image, (landmark[ii * 2 + 0], landmark[ii * 2 + 1]), 2, (0, 255, 0), -1)

    if show: cv2.imshow("zxczxc", landmark_image)


    left_eye = [landmark[39 * 2 + 0], landmark[39 * 2 + 1]]
    right_eye = [landmark[42 * 2 + 0], landmark[42 * 2 + 1]]
    nose_bridge = [landmark[30 * 2 + 0], landmark[30 * 2 + 1]]

    (left_eye_x, left_eye_y) = left_eye
    (right_eye_x, right_eye_y) = right_eye
    (c_x, c_y) = nose_bridge

    height_value = right_eye_x - left_eye_x
    per_height_bottom_value = height_value * 13

    rect_upper = (int(c_x - per_height_bottom_value / 2), int(c_y - per_height_bottom_value / 2))
    rect_bottom = (int(c_x + per_height_bottom_value / 2), int(c_y + per_height_bottom_value / 2))

    ux, uy = rect_upper
    bx, by = rect_bottom

    if by > im_height:
        target = by - im_height
        image = cv2.copyMakeBorder(image, top=0, bottom=target, left=0, right=0,
                                   borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255))
    if uy < 0:
        target = abs(uy)
        image = cv2.copyMakeBorder(image, top=target, bottom=0, left=0, right=0,
                                   borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255))
        by = by + target
        uy = 0

    if ux < 0:
        target = abs(ux)
        image = cv2.copyMakeBorder(image, top=0, bottom=0, left=target, right=0,
                                   borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255))
        bx = bx + target
        ux = 0

    if bx > im_width:
        target = bx - im_width
        image = cv2.copyMakeBorder(image, top=0, bottom=0, left=0, right=target,
                                   borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255))

    crop_img = image[uy:by, ux:bx]
    crop_height, crop_width, crop_channel = crop_img.shape

    mask_border = crop_img

    if crop_height < 224 and crop_width < 224:
        complete = cv2.resize(mask_border, dsize=(224, 224), interpolation=cv2.INTER_CUBIC)
    else:
        complete = cv2.resize(mask_border, dsize=(224, 224), interpolation=cv2.INTER_AREA)

    return complete

