#-*-coding: utf-8

import torch
import torch.nn as nn
from torchvision import datasets, models, transforms
from torch.nn.functional import interpolate
import torch.nn.functional as F
import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from preprocess_for_cam import cam_preocess
from net.MobileNetV2_unet import MobileNetV2_unet
import time


deep_model_dir = './model/IOU0.7733821534051539_checkpoint5.pth'
test_model_dir = './model/testing/new/valid_accuracy[0.9657143]_train_accuracy[0.99997956]_checkpoint184.pth'


class HairInfoRunner:
    def __init__(self, args):
        self.device = torch.device("cuda:{}".format(args.device) if torch.cuda.is_available() else "cpu")

        self.model = models.resnet18(pretrained=False)
        in_features = self.model.fc.in_features
        self.model.fc = nn.Linear(in_features=in_features, out_features=5)
        self.model = self.model.to(self.device)
        self.model.load_state_dict(torch.load(test_model_dir, map_location=self.device))
        self.model.eval()
        print(self.model)

        self.deep_net = MobileNetV2_unet(None)
        self.deep_net = self.deep_net.to(self.device)
        self.deep_net.load_state_dict(torch.load(deep_model_dir, map_location=self.device))
        print("model loaded from {}".format(deep_model_dir))
        self.deep_net.eval()

        self.transform_seg_data = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            # transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])

        self.hair_color_transforms = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

    def run(self, img_bytes):
        image = cv2.imdecode(np.frombuffer(img_bytes, np.uint8), -1)

        converted_deep_input_image = cam_preocess(image, show=False)

        if isinstance(converted_deep_input_image, int): # detection or landmark 감지 실패
            return False

        seg_input = converted_deep_input_image.copy()
        seg_input = cv2.cvtColor(seg_input, cv2.COLOR_BGR2RGB)
        seg_input = Image.fromarray(seg_input)
        seg_input = self.transform_seg_data(seg_input)
        seg_input = torch.unsqueeze(seg_input, 0)
        seg_input = seg_input.to(self.device)

        seg_result = self.deep_net(seg_input)
        seg_result = interpolate(seg_result, scale_factor=2, mode='bilinear', align_corners=False)
        seg_result[seg_result > 0.5] = 255
        seg_result[seg_result <= 0.5] = 0
        np_seg_result = seg_result.cpu().detach().numpy()

        np_seg_result = np.transpose(np_seg_result, (2, 3, 1, 0))
        np_seg_result = np_seg_result[:, :, 0, 0]

        image_original_for_hair_color = converted_deep_input_image.copy()
        image_original_for_hair_color[np_seg_result != 255] = 0

        hair_color_deep_input = cv2.cvtColor(image_original_for_hair_color, cv2.COLOR_BGR2RGB)
        hair_color_deep_input = Image.fromarray(hair_color_deep_input)
        hair_color_deep_input = self.hair_color_transforms(hair_color_deep_input)
        hair_color_deep_input = torch.unsqueeze(hair_color_deep_input, 0)
        hair_color_deep_input = hair_color_deep_input.to(self.device)

        result_str = 'None'
        with torch.set_grad_enabled(False):
            hair_color_result = self.model(hair_color_deep_input)

            sm_color_result = F.log_softmax(hair_color_result, dim=1)
            _, preds = torch.max(sm_color_result, 1)
            final_result = preds.cpu().detach().numpy()

            if final_result == 0:
                result_str = "Black Hair"
            elif final_result == 1:
                result_str = "Blond Hair"
            elif final_result == 2:
                result_str = "Red Hair"
            elif final_result == 3:
                result_str = "Pink Hair"
            elif final_result == 4:
                result_str = "Brown Hair"

            _, result_image_bytes = cv2.imencode(".jpg", image_original_for_hair_color)
        return result_str, result_image_bytes.tobytes()


if __name__ == '__main__':
    hair_info_runner = HairInfoRunner()
    with open("a.jpg", "rb") as f:
        info, result_image_file = hair_info_runner.run(f.read())
        print(info)
        with open("a_out.jpg", "wb") as fa:
            fa.write(result_image_file)
