import argparse
import time
import grpc

from concurrent import futures

from hair_info_pb2 import OutputImageWithInfo
from hair_info_pb2_grpc import HairInfoServicer, add_HairInfoServicer_to_server

from runner import HairInfoRunner


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--device", default=0)
    parser.add_argument("--chunk_size", default=4096)
    parser.add_argument("--port", default=15001)

    return parser.parse_args()


class HairInfoServer(HairInfoServicer):
    def __init__(self, args):
        self.res_chunk_size = args.chunk_size
        self.runner = HairInfoRunner(args)

    def GetHairInfo(self, request_iterator, context):
        request = b""

        for request_part in request_iterator:
            request += request_part.input

        result_string, result_img = self.runner.run(request)

        yield OutputImageWithInfo(info=result_string)

        for idx in range(0, len(result_img), self.res_chunk_size):
            yield OutputImageWithInfo(
                image=result_img[idx:idx + self.res_chunk_size]
            )


if __name__ == '__main__':
    args = parse_args()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))

    hair_info_server = HairInfoServer(args=args)
    hair_info_servicer = add_HairInfoServicer_to_server(hair_info_server, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    server.start()

    print("hair info grpc server starting at 0.0.0.0:{}".format(args.port))

    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)
