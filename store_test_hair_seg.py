from net.MobileNetV2_unet import MobileNetV2_unet
import os
import torch
from deep_seg_only_image_custom_dataset import divide_id, split_train_val_test, HairDataset
from torch.nn.functional import interpolate
import numpy as np
import cv2

test_dir = 'E:/hair_dataset/hair_color/about_color/old_construct/totally_process/valid/processImage/2_process_image/'
test_model_dir = "./model/0814DC_HairModel_transform.pth"
save_result_dir = "E:/hair_dataset/hair_color/about_color/old_construct/totally_process/valid/processImage/2_process_mask_image/"

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def test():
    if not os.path.exists(test_dir) or not os.path.isdir(test_dir):
        raise ValueError('입력 이미지 경로가 올바르지 않습니다.(%s)' % test_dir)

    input_id_divided = divide_id(test_dir)
    list_id = [f[:-4] for f in os.listdir(test_dir)]
    input_dataset = split_train_val_test(input_id_divided)
    face_dataset = HairDataset(input_dataset['test'], test_dir)
    face_loader = torch.utils.data.DataLoader(face_dataset, batch_size=1, shuffle=False, num_workers=1)

    net = MobileNetV2_unet(None)
    net = net.to(device)
    net.load_state_dict(torch.load(test_model_dir))
    print("model loaded from {}".format(test_model_dir))
    net.eval()
    count = 0

    for i, dataset in enumerate(face_loader):
        input = dataset['image']
        input = input.to(device)

        with torch.set_grad_enabled(False):
            mask_pred = net(input)
            mask_pred = interpolate(mask_pred, scale_factor=2, mode='bilinear', align_corners=False)
            mask_pred[mask_pred > 0.5] = 255
            mask_pred[mask_pred <= 0.5] = 0
            np_img = mask_pred.cpu().detach().numpy()
            np_img = np.transpose(np_img, (2, 3, 1, 0))
            np_img = np_img[:, :, 0, 0]
            ids = list_id[count]

            cv2.imwrite(save_result_dir + ids + "_mask.png", np_img)
            print(save_result_dir + ids + "_mask.png이 저장되었습니다.")
            print("진행상황 : %d/%d" % ((count + 1), len(list_id)))
            count = count + 1







if __name__ == '__main__':
    test()